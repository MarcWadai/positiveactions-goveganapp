import 'package:flutter/material.dart';

const List<Object> foodType = [
  {"id": "1", "food_type": "Vegan"},
  {"id": "2", "food_type": "Buddhist"},
  {"id": "3", "food_type": "Vegetarian"},
  {"id": "4", "food_type": "Has vegan option"}
];

const Map<int, Color> colorButton =
{
  0:Color.fromRGBO(227,96,104, 1),
  1:Color.fromRGBO(156,194,94, 1),
  2:Color.fromRGBO(255,228,109, 1),
  3:Color.fromRGBO(125,205,244, 1),
};

const double mapLatOrigin = 25.0330;
const double mapLngOrigin = 121.5654;