import 'dart:io';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableRestaurant = 'restaurants';
final String columnId = 'id';
final String columnTitle = 'title';
final String columnFoodType = 'food_type';
final String columnLat = 'lat';
final String columnLon= 'lon';
final String columnCity = 'city';
final String columnPhotoUrl= 'photo_url';


// singleton class to manage the database
class DatabaseHelper {

  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "goveganapp.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableRestaurant (
                $columnId TEXT PRIMARY KEY,
                $columnTitle TEXT NOT NULL,
                $columnFoodType TEXT NOT NULL,
                $columnLat TEXT,
                $columnLon TEXT,
                $columnCity TEXT NOT NULL,
                $columnPhotoUrl TEXT NOT NULL
              )
              ''');
  }

  // Database helper methods:

  Future<int> insert(Restaurant restaurant) async {
    Database db = await database;
    int id = await db.insert(tableRestaurant, restaurant.toMap());
    return id;
  }

  Future<Restaurant> queryRestaurant(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableRestaurant,
        columns: [columnId, columnTitle,
        columnFoodType,
        columnLat,
        columnLon,
        columnCity,
        columnPhotoUrl],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Restaurant.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Restaurant>> queryAll() async {
    Database db = await database;
    List<Map> maps = await db.query(tableRestaurant);
    List<Restaurant> restaurantArray = [];
    if (maps.length > 0) {
      for (var i = 0 ; i < maps.length; i++) {
        restaurantArray.add(Restaurant.fromMap(maps[i]));
      }
      return restaurantArray;
    }
    return null;
  }

  Future<int> deleteRestaurant(String id) async {
    Database db = await database;
    return await db.delete(tableRestaurant, where: '$columnId = ?', whereArgs: [id]);
  }

}