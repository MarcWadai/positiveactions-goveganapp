
import 'package:flutter/material.dart';

final ThemeData CompanyThemeData = new ThemeData(
    brightness: Brightness.light,
    primarySwatch:  MaterialColor(CompanyColors.orange[50].value, CompanyColors.orange),
    primaryColor: CompanyColors.orange[500],
    primaryColorBrightness: Brightness.light,
    accentColor: CompanyColors.orange[700],
    accentColorBrightness: Brightness.light
);

class CompanyColors {
  CompanyColors._(); // this basically makes it so you can instantiate this class
  static const Map<int, Color> orange = const <int, Color> {
    50:Color.fromRGBO(240, 150, 0, 1),
    100:Color.fromRGBO(240, 150, 0, 1),
    200:Color.fromRGBO(240, 150, 0, 1),
    300:Color.fromRGBO(240, 150, 0, 1),
    400:Color.fromRGBO(240, 150, 0, 1),
    500:Color.fromRGBO(240, 150, 0, 1),
    600:Color.fromRGBO(240, 150, 0, 1),
    700:Color.fromRGBO(240, 150, 0, 1),
    800:Color.fromRGBO(240, 150, 0, 1),
    900:Color.fromRGBO(240, 150, 0, 1),
  };

}