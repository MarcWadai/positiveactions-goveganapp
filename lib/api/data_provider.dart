import 'dart:convert';

import 'package:go_vegan_app/env.dart';
import 'package:go_vegan_app/models/News.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:go_vegan_app/models/RestaurantDetail.dart';
import 'package:http/http.dart' as http;

class DataProvider {
  final http.Client httpClient = http.Client();

  final host = Env.value.baseUrl;
  Future<List<Restaurant>> getRestaurants() async {
    // Read from DB or make network request etc...
    print('started http call');
    final response = await httpClient.get('$host/restaurants.php');
    print('after http call $response');
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      return data.map((rawPost) {
        return Restaurant.fromJson(rawPost);
      }).toList();
    } else {
      throw Exception('error fetching posts $response');
    }
  }

  Future<RestaurantDetail> getRestaurantsById(id) async {
    final response = await httpClient.get('$host/restaurants.php?restID=$id');
    print('after http call $response');
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      if (data.length > 0) {
        print('data restaurantDetail $data');
        return RestaurantDetail.fromJson(data[0]);
      }
      return null;
    } else {
      throw Exception('error fetching posts $response');
    }
  }

  Future<List<News>> getNews() async {
    final response = await httpClient.get('$host/news.php');
    print('response for news $response');
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      if (data.length > 0) {
        return data.map((rawPost) {

          return News.fromJson(rawPost);
        }).toList();
      }
      return null;
    } else {
      throw Exception('error fetching posts $response' );
    }
  }
}