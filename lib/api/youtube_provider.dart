import 'dart:convert';

import 'package:go_vegan_app/env.dart';
import 'package:go_vegan_app/models/News.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:go_vegan_app/models/RestaurantDetail.dart';
import 'package:http/http.dart' as http;

class YoutubeProvider {
  final http.Client httpClient = http.Client();

  final host = Env.value.youtubeBaseURL;
  final key = Env.value.googleAPIKey;


  Future<String> getYTThumbnail(videoId) async {
    final response = await httpClient.get('$host?key=$key&id=$videoId&part=snippet');
    print('response for yttthumbnail $response');
    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      print('response for yttthumbnail data $data');
      return _getThumbnailInfo(data);
    } else {
      final error = json.decode(response.body);
      print('response for yttthumbnail data $error');
      throw Exception('error fetching posts $error' );
    }
  }


  String _getThumbnailInfo(Map ytObj) {
    /*String thumbnailUrl = "";
    ytObj.keys.forEach((key) {
      if (key == 'items' && ytObj[key].length > 0) {
        thumbnailUrl = ytObj[key][0][]snippet.thumbnails.medium.url;
      }
    });*/
    return ytObj['items'][0]['snippet']['thumbnails']['medium']['url'];
  }

}