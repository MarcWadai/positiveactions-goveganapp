import 'package:flutter/material.dart';
import 'package:go_vegan_app/config/development.dart';
import 'package:go_vegan_app/env.dart';
import 'package:go_vegan_app/pages/home.dart';
import 'package:go_vegan_app/theme.dart' as Theme;

void main() => Development();

Map<int, Color> color =
{
  50:Color.fromRGBO(240, 150, 0, 1),
  100:Color.fromRGBO(240, 150, 0, 1),
  200:Color.fromRGBO(240, 150, 0, 1),
  300:Color.fromRGBO(240, 150, 0, 1),
  400:Color.fromRGBO(240, 150, 0, 1),
  500:Color.fromRGBO(240, 150, 0, 1),
  600:Color.fromRGBO(240, 150, 0, 1),
  700:Color.fromRGBO(240, 150, 0, 1),
  800:Color.fromRGBO(240, 150, 0, 1),
  900:Color.fromRGBO(240, 150, 0, 1),
};


class MyApp extends StatelessWidget {
  final Env env;
  MyApp(this.env);
  MaterialColor colorCustom = MaterialColor(color[50].value, color);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Go vegan app',
      color: Colors.white,
      theme:  Theme.CompanyThemeData,
        //ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
       // primarySwatch: colorCustom
      //),
      home: HomePage(title: 'Go Vegan Home Page'),
    );
  }
}
