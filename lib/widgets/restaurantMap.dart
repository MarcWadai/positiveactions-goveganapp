import 'package:flutter/material.dart';
import 'package:go_vegan_app/pages/restaurantsSearch.dart';

class RestaurantMap extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new RestaurantMapState();
  }
}

class RestaurantMapState extends State<RestaurantMap> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
          top: true,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ButtonTheme(
                minWidth: 70.0,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.white,
                ),
              ),
              ButtonTheme(
                minWidth: 70.0,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.greenAccent,
                ),
              ),
              ButtonTheme(
                minWidth: 70.0,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.yellowAccent,
                ),
              ),
              ButtonTheme(
                minWidth: 70.0,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.orangeAccent,
                ),
              ),
              Icon(Icons.search)
            ],
          ),
        ));
  }
}
