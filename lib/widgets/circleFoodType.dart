import 'package:flutter/material.dart';
import 'package:go_vegan_app/helpers/constants.dart' as Constants;

class CircleFoodType extends StatelessWidget{
  final String foodType;

  CircleFoodType(
      {Key key, @required this.foodType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var color = Colors.white;
    switch (foodType) {
      case "1":
        color = Constants.colorButton[0];
        break;
      case "2":
        color = Constants.colorButton[1];
        break;
      case "3":
        color = Constants.colorButton[2];
        break;
      case "4":
        color = Constants.colorButton[3];
        break;
    }
    return Container(
      width: 10.0,
      height: 10.0,
      decoration: new BoxDecoration(
          color: color,
          shape: BoxShape.circle,
          border: foodType == "1" ? Border.all(color: Colors.black) : null),
    );
  }
}