import 'package:flutter/material.dart';
import 'package:go_vegan_app/bloc/restaurant_search/restaurant_search_index.dart';
import 'package:go_vegan_app/bloc/restaurant_search/restaurants_search_bloc.dart';
import 'package:go_vegan_app/bloc/restaurant_search/restaurants_search_event.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:go_vegan_app/helpers/constants.dart' as Constants;

class SearchBar extends StatefulWidget {
  final List<Restaurant> originalRestaurant;
  final Function(String, List<Restaurant>, [String]) callback;

  SearchBar(
      {Key key, @required this.originalRestaurant, @required this.callback})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new SearchBarState();
  }
}

class SearchBarState extends State<SearchBar> {
  Icon actionIcon = new Icon(Icons.search, color: Colors.white,);
  Widget appBarTitle;
  final double buttonWidth = 80;
  int currentButton = 0;
  Map<int, Color> colorButton = Constants.colorButton;
  double borderButton = 7;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: actionIcon.icon == Icons.search
            ? _buildButtons(currentButton)
            : _buildInput(),
        titleSpacing: 2,
        actions: <Widget>[
          new IconButton(
            icon: actionIcon,
            onPressed: () {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = new Icon(Icons.close);
                } else {
                  widget.callback('RESET', widget.originalRestaurant);
                  this.actionIcon = new Icon(Icons.search);
                  currentButton = 0;
                }
              });
            },
          ),
        ]);
  }

  void _manageFilterButton(int buttonId) {
    if (currentButton == buttonId) {
      widget.callback('RESET', widget.originalRestaurant);
      currentButton = 0;
    } else {
      widget.callback('BUTTON', widget.originalRestaurant, buttonId.toString());
      currentButton = buttonId;
    }
  }

  void _manageFilterText(String textFilter) {
    if (textFilter == "") {
      widget.callback('RESET', widget.originalRestaurant);
    } else {
      widget.callback('TEXT', widget.originalRestaurant, textFilter);
    }
  }

  Widget _buildInput() {
    return new TextField(
      style: new TextStyle(
        color: Colors.white,
      ),
      onSubmitted: (String value) {
        setState(() {
          _manageFilterText(value);
        });
      },
      decoration: new InputDecoration(
          prefixIcon: new Icon(Icons.search, color: Colors.white),
          hintText: "Search...",
          hintStyle: new TextStyle(color: Colors.white)),
    );
  }

  Widget _buildButtons(currentButton) {
    return Container(
        child: SafeArea(
      top: true,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: ButtonTheme(
            minWidth: buttonWidth,
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderButton)),
              onPressed: () {
                setState(() {
                  _manageFilterButton(1);
                });
              },
              child: Center(
                  child: Text(
                '全素',
                style: currentButton == 1 || currentButton == 0 ? TextStyle(color: Colors.white) : TextStyle(color: Colors.black),
              )),
              color: currentButton == 1 || currentButton == 0 ? colorButton[0] : Colors.white ,
            ),
          )),
          Expanded(
              child: ButtonTheme(
            minWidth: buttonWidth,
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderButton)),
              onPressed: () {
                setState(() {
                  _manageFilterButton(2);
                });
              },
              child: Center(
                child: Text('純素', style: currentButton == 2 || currentButton == 0 ? TextStyle(color: Colors.white) :  TextStyle(color: Colors.black)),
              ),
              color: currentButton == 2 || currentButton == 0 ? colorButton[1] : Colors.white,
            ),
          )),
          Expanded(
              child: ButtonTheme(
            minWidth: buttonWidth,
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderButton)),
              onPressed: () {
                setState(() {
                  _manageFilterButton(3);
                });
              },
              child: Center(
                  child: Text(
                '蛋奶',
                textScaleFactor: 1.0,
                //style: currentButton == 3 || currentButton == 0 ? TextStyle(color: Colors.white) : TextStyle(color: Colors.black),
              )),
              color: currentButton == 3 || currentButton == 0 ? colorButton[2] : Colors.white,
            ),
          )),
          Expanded(
              child: ButtonTheme(
            minWidth: buttonWidth,
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderButton)),
              onPressed: () {
                setState(() {
                  _manageFilterButton(4);
                });
              },
              child: Center(
                  child: Text(
                '素食友善',
                style: currentButton == 4 || currentButton == 0 ? TextStyle(color: Colors.white) : TextStyle(color: Colors.black),
              )),
              color: currentButton == 4 || currentButton == 0 ? colorButton[3] : Colors.white,
            ),
          )),
        ],
      ),
    ));
  }
}
