import 'package:go_vegan_app/api/youtube_provider.dart';
import 'package:go_vegan_app/bloc/news/news_bloc.dart';
import 'package:go_vegan_app/env.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';

class MyVideoPlayer extends StatefulWidget {
  final String ytURL;

  MyVideoPlayer({Key key, @required this.ytURL}) : super(key: key);

  @override
  _MyVideoPlayerState createState() => _MyVideoPlayerState();
}

class _MyVideoPlayerState extends State<MyVideoPlayer> {
  final YoutubeProvider ytProvider = YoutubeProvider();

  String thumbnailUrl = "";

  @override
  void initState() {
    super.initState();
    if (widget.ytURL != null) {
      ytProvider.getYTThumbnail(_youtubeUrlToId()).then((value) {
        setState(() {
          thumbnailUrl = value;
        });
      }).catchError((error) {
        print(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          _playVideo();
        },
        child: Container(
          constraints: new BoxConstraints.expand(
            height: 200.0,
          ),
          padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: NetworkImage(thumbnailUrl),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Icon(Icons.play_circle_outline,
                color: Color.fromRGBO(240, 150, 0, 1), size: 70),
          ),
        ));
  }

  //  getting the video id from the youtube url
  String _youtubeUrlToId() {
    return widget.ytURL.split('=').last;
  }

  void _playVideo() {
    FlutterYoutube.playYoutubeVideoByUrl(
        apiKey: Env.value.googleAPIKey,
        videoUrl: widget.ytURL,
        autoPlay: false, //default falase
        fullScreen: true //default false
    );

  }
}
