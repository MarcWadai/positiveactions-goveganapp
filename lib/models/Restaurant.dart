
import 'package:equatable/equatable.dart';
import 'package:go_vegan_app/models/OpeningDays.dart';

class Restaurant extends Equatable{
  final String id;
  final String title;
  final String food_type;
  final String lat;
  final String lon;
  final String city;
  final String photo_url;
  final List<OpeningDays> openingDays;


  Restaurant({this.id,
      this.title,
      this.food_type,
      this.lat,
      this.lon,
      this.city,
      this.photo_url,
      this.openingDays}) : super([id, title, food_type, lat, lon, city, photo_url, openingDays]);

  factory Restaurant.fromJson(Map<String, dynamic> json){
    var openingDaysList = json['opening_hours'] as List;
    List<OpeningDays> openingDaysJSON = openingDaysList.map( (i) {
      return OpeningDays.fromJson(i);}).toList();

    return Restaurant(
        id: json['id'],
        title: json['title'],
        food_type: json['food_type'],
        lat: json['lat'],
        lon: json['lon'],
        city: json['city'],
        photo_url: json['photo_url'],
        openingDays: openingDaysJSON
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "food_type": food_type,
    "lat": lat,
    "lon": lon,
    "city": city,
    "photo_url": photo_url
  };

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "id": id,
      "title": title,
      "food_type": food_type,
      "lat": lat,
      "lon": lon,
      "city": city,
      "photo_url": photo_url
    };
    if (id != null) {
      map["id"] = id;
    }
    return map;
  }

  // convenience constructor to create a Word object
  factory Restaurant.fromMap(Map<String, dynamic> map) {
    return Restaurant(
      id: map["id"],
      title: map["title"],
      food_type: map["food_type"],
      lat: map["lat"],
      lon: map["lon"],
      city: map["city"],
      photo_url: map["photo_url"],
    );
  }

}