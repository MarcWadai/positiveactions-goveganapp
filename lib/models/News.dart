
import 'package:equatable/equatable.dart';

class News extends Equatable{
  final String id;
  final String post_title;
  final String post_date;
  final String youtube_link;

  News({this.id,
    this.post_title,
    this.post_date,
    this.youtube_link
  }) : super([id, post_title, post_date, youtube_link]);

  factory News.fromJson(Map<String, dynamic> json){
    return News(
        id: json['id'],
        post_title: json['post_title'],
        post_date: json['post_date'],
        youtube_link: json['youtube_link']
    );
  }

}