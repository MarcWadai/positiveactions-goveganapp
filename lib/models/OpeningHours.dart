
import 'package:equatable/equatable.dart';

class OpeningHours extends Equatable{
  final String id;
  final String open_time;
  final String close_time;
  final String opening_day;
  final String closed;

  OpeningHours({this.id,
    this.open_time,
    this.close_time,
    this.opening_day,
    this.closed
  }) : super([id, open_time, close_time, opening_day, closed]);

  factory OpeningHours.fromJson(Map<String, String> json){
    return OpeningHours(
        id: json['id'],
        open_time: json['open_time'],
        close_time: json['close_time'],
        opening_day: json['opening_day'],
        closed: json['closed']
    );
  }

}