
import 'package:equatable/equatable.dart';
import 'package:go_vegan_app/models/OpeningHours.dart';

class OpeningDays extends Equatable{
  final String id;
  final String day_start;
  final String day_end;
  final String restaurant;
  final List<OpeningHours> hours;


  OpeningDays({this.id,
    this.day_start,
    this.day_end,
    this.restaurant,
    this.hours
  }) : super([id, day_start, day_end, restaurant, hours]);

  factory OpeningDays.fromJson(Map<String, dynamic> json){
    var openingHoursList = json['hours'] as List;
    List<OpeningHours> openingHours = openingHoursList.map((i) {return OpeningHours.fromJson(Map<String, String>.from(i));}).toList();
    return OpeningDays(
        id: json['id'],
        day_start: json['day_start'],
        day_end: json['day_end'],
        restaurant: json['restaurant'],
        hours: openingHours
    );
  }

}