
import 'package:equatable/equatable.dart';
import 'package:go_vegan_app/models/OpeningDays.dart';
import 'package:go_vegan_app/models/Photo.dart';

class RestaurantDetail extends Equatable{
  final String id;
  final String title;
  final String food_type;
  final String lat;
  final String lon;
  final String city;
  final List<Photo> photos;
  final String address;
  final String youtube_link;
  final List<OpeningDays> opening_hours;


  RestaurantDetail({this.id,
    this.title,
    this.food_type,
    this.lat,
    this.lon,
    this.city,
    this.photos,
    this.address,
    this.opening_hours,
    this.youtube_link}) : super([id, title, food_type, lat, lon, city, photos, address, opening_hours, youtube_link]);


  factory RestaurantDetail.fromJson(Map<String, dynamic> json){
    var photosFromList = json['photos'] as List;
    var openingDaysList = json['opening_hours'] as List;
    List<OpeningDays> openingDaysJSON = openingDaysList.map( (i) {
      return OpeningDays.fromJson(i);}).toList();
    List<Photo> photos = photosFromList.map((i) {
      return Photo.fromJson(Map<String, String>.from(i));}).toList();
    return RestaurantDetail(
        id: json['id'],
        title: json['title'],
        food_type: json['food_type'],
        lat: json['lat'],
        lon: json['lon'],
        city: json['city'],
        photos: photos,
        address: json['address'],
        youtube_link: json['youtube_link'],
        opening_hours: openingDaysJSON
    );
  }
}