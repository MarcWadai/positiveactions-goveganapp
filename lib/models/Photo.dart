
import 'package:equatable/equatable.dart';

class Photo extends Equatable{
  final String photo_name;
  final String big_url;
  final String small_url;


  Photo({this.photo_name,
    this.big_url,
    this.small_url,
    }) : super([photo_name, big_url, small_url]);

  factory Photo.fromJson(Map<String, String> json){
    return Photo(
        photo_name: json['photo_name'],
        big_url: json['big_url'],
        small_url: json['small_url']
    );
  }

}