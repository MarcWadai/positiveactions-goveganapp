import 'package:equatable/equatable.dart';
import 'package:go_vegan_app/models/Restaurant.dart';

abstract class RestaurantSearchEvent extends Equatable {}

class GetRestaurants extends RestaurantSearchEvent {
  @override
  String toString() => 'GetRestaurants';
}

class FilterWithButton extends RestaurantSearchEvent {
  int filterButtonId;
  List<Restaurant> originalList;
  FilterWithButton(int id, List<Restaurant> originalList) {
    this.filterButtonId = id;
    this.originalList = originalList;
  }

  @override
  String toString() => 'FilterWithButton';
}

class FilterWithText extends RestaurantSearchEvent {
  String filterText;
  List<Restaurant> originalList;

  FilterWithText(String text, List<Restaurant> originalList) {
    this.filterText = text;
    this.originalList = originalList;
  }

  @override
  String toString() => 'FilterWithText';
}

class FilterReset extends RestaurantSearchEvent {
  List<Restaurant> originalList;

  FilterReset(List<Restaurant> originalList) {
    this.originalList = originalList;
  }

  @override
  String toString() => 'FilterReset';
}

