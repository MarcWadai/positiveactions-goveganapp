import 'package:go_vegan_app/bloc/restaurant_search/restaurants_search_event.dart';
import 'package:go_vegan_app/bloc/restaurant_search/restaurants_search_state.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:bloc/bloc.dart';
import 'package:go_vegan_app/api/data_provider.dart';

class RestaurantSearchBloc
    extends Bloc<RestaurantSearchEvent, RestaurantSearchState> {
  final DataProvider dataProvider = DataProvider();

  RestaurantSearchBloc();

  @override
  // TODO: implement initialState
  RestaurantSearchState get initialState => SearchUninitialized();

  @override
  Stream<RestaurantSearchState> mapEventToState(
    RestaurantSearchState currentState,
    RestaurantSearchEvent event,
  ) async* {
    if (event is GetRestaurants) {
      try {
        if (currentState is SearchUninitialized) {
          final restaurants = await dataProvider.getRestaurants();
          yield SearchLoaded(restaurants: restaurants);
        }
        if (currentState is SearchLoaded) {
          yield SearchLoaded(
                  restaurants: currentState.restaurants);
        }
      } catch (_) {
        print(_);
        yield SearchError();
      }
    }
    if (event is FilterWithText) {
      List<Restaurant> restFiltered = event.originalList.where( (r) {

        return r.title.contains(event.filterText);
      }).toList();
      yield SearchFilter(restaurantFilter: restFiltered);
    }

    if (event is FilterWithButton) {
      List<Restaurant> restFilteredButton = event.originalList.where( (r) {
        return r.food_type == event.filterButtonId.toString();
      }).toList();
      yield SearchFilter(restaurantFilter: restFilteredButton, filterButtonId: event.filterButtonId);
    }


    if (event is FilterReset) {
      yield SearchFilter(restaurantFilter: event.originalList);
    }
  }
}
