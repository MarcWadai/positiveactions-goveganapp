import 'package:equatable/equatable.dart';
import 'package:go_vegan_app/models/Restaurant.dart';

abstract class RestaurantSearchState extends Equatable {
  RestaurantSearchState([List props = const []]) : super(props);
}

class SearchUninitialized extends RestaurantSearchState {
  @override
  String toString() => 'SearchUninitialized';
}

class SearchError extends RestaurantSearchState {
  @override
  String toString() => 'SearchError';
}

class SearchLoaded extends RestaurantSearchState {
  final List<Restaurant> restaurants;

  SearchLoaded({
    this.restaurants
  }) : super([restaurants]);

  SearchLoaded copyWith({
    List<Restaurant> restaurants,
  }) {
    return SearchLoaded(
      restaurants: restaurants ?? this.restaurants,
    );
  }
}

class SearchFilter extends RestaurantSearchState {
  final List<Restaurant> restaurantFilter;
  final int filterButtonId;

  SearchFilter({
    this.restaurantFilter,
    this.filterButtonId
  }) : super([restaurantFilter, filterButtonId]);

  SearchFilter copyWith({
    List<Restaurant> restaurantFilter,
    int filterButtonId
  }) {
    return SearchFilter(
      restaurantFilter: restaurantFilter ?? this.restaurantFilter,
        filterButtonId: filterButtonId ?? this.filterButtonId
    );
  }

  @override
  String toString() => 'SearchFilter';
}