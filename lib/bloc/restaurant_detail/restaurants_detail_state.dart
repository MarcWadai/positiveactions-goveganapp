import 'package:equatable/equatable.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:go_vegan_app/models/RestaurantDetail.dart';

abstract class RestaurantDetailState extends Equatable {
  RestaurantDetailState ([List props = const []]) : super(props);
}

class DetailUninitialized extends RestaurantDetailState {
  @override
  String toString() => 'DetailUninitialized';
}

class DetailError extends RestaurantDetailState {
  @override
  String toString() => 'DetailError';
}

class DetailLoaded extends RestaurantDetailState {
  final RestaurantDetail restaurantDetail;

  DetailLoaded({
    this.restaurantDetail
  }) : super([restaurantDetail]);

  DetailLoaded copyWith({
    RestaurantDetail restaurant,
  }) {
    return DetailLoaded(
      restaurantDetail: restaurant ?? this.restaurantDetail,
    );
  }
}