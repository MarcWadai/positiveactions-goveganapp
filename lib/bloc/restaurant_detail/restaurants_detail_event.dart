import 'package:equatable/equatable.dart';

abstract class RestaurantDetailEvent extends Equatable {}

class GetRestaurantDetail extends RestaurantDetailEvent {

  String id;

  GetRestaurantDetail(String id) {
    this.id = id;
  }

  @override
  String toString() => 'GetRestaurantDetail';
}

