import 'package:go_vegan_app/bloc/restaurant_detail/restaurants_detail_event.dart';
import 'package:go_vegan_app/bloc/restaurant_detail/restaurants_detail_state.dart';
import 'package:bloc/bloc.dart';
import 'package:go_vegan_app/api/data_provider.dart';

class RestaurantDetailBloc
    extends Bloc<RestaurantDetailEvent, RestaurantDetailState> {
  final DataProvider dataProvider = DataProvider();

  RestaurantDetailBloc();

  @override
  // TODO: implement initialState
  RestaurantDetailState get initialState => DetailUninitialized();

  @override
  Stream<RestaurantDetailState > mapEventToState(
      RestaurantDetailState  currentState,
      RestaurantDetailEvent   event,
  ) async* {
    if (event is GetRestaurantDetail) {
      try {
        if (currentState is DetailUninitialized) {
          print('in bloc detail $currentState');
          final detail = await dataProvider.getRestaurantsById(event.id);
          print('in bloc detail after request $detail');
          yield DetailLoaded(restaurantDetail: detail);
        }
        if (currentState is DetailLoaded) {
          yield DetailLoaded(
              restaurantDetail: currentState.restaurantDetail);
        }
      } catch (_) {
        print(_);
        yield DetailError();
      }
    }
  }
}
