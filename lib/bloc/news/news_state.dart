import 'package:equatable/equatable.dart';
import 'package:go_vegan_app/models/News.dart';

abstract class NewsState extends Equatable {
  NewsState ([List props = const []]) : super(props);
}

class NewsUninitialized extends NewsState {
  @override
  String toString() => 'NewsUninitialized';
}

class NewsError extends NewsState {
  @override
  String toString() => 'NewsError';
}

class NewsLoaded extends NewsState {
  final List<News> newsList;

  NewsLoaded({
    this.newsList
  }) : super([newsList]);

  NewsLoaded copyWith({
    List<News> newsList,
  }) {
    return NewsLoaded(
      newsList: newsList ?? this.newsList,
    );
  }
}

/*
class NewsPlayVideo extends NewsState {
  @override
  String toString() => 'NewsPlayVideo';
}

class NewsPlayVideoUninitialized extends NewsState {

  final String urlThumbnail;

  NewsPlayVideoUninitialized({
    this.urlThumbnail
  }) : super([urlThumbnail]);

  NewsPlayVideoUninitialized copyWith({
    String urlThumbnail,
  }) {
    return NewsPlayVideoUninitialized(
      urlThumbnail: urlThumbnail ?? this.urlThumbnail,
    );
  }

  @override
  String toString() => 'NewsPlayVideoUninitialized';
}*/