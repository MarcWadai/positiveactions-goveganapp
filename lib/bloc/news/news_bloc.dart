import 'package:bloc/bloc.dart';
import 'package:go_vegan_app/api/data_provider.dart';
import 'package:go_vegan_app/api/youtube_provider.dart';
import 'package:go_vegan_app/bloc/news/news_event.dart';
import 'package:go_vegan_app/bloc/news/news_state.dart';

class NewsBloc
    extends Bloc<NewsEvent, NewsState> {
  final DataProvider dataProvider = DataProvider();
  final YoutubeProvider youtubeProvider = YoutubeProvider();

  NewsBloc();

  @override
  // TODO: implement initialState
  NewsState get initialState => NewsUninitialized();

  @override
  Stream<NewsState > mapEventToState(
      NewsState  currentState,
      NewsEvent   event,
  ) async* {
    if (event is GetNews) {
      try {
        if (currentState is NewsUninitialized) {
          final newsList = await dataProvider.getNews();
          yield NewsLoaded(newsList: newsList);
        }
        if (currentState is NewsLoaded) {
          yield NewsLoaded(
              newsList: currentState.newsList);
        }
      } catch (_) {
        print(_);
        yield NewsError();
      }
    }

/*    if (event is PlayVideoThumbnail) {
      try {
        if (currentState is NewsPlayVideoUninitialized) {
          if (currentState.urlThumbnail == null) {
            final imageUrl = await youtubeProvider.getYTThumbnail(event.id);
            yield NewsPlayVideoUninitialized(urlThumbnail: imageUrl);
          } else {
            NewsPlayVideoUninitialized(urlThumbnail: currentState.imageUrl);
          }

        }
      } catch (_) {
        print(_);
        yield NewsPlayVideoUninitialized(urlThumbnail: null);
      }
    }

    if (event is PlayVideoClick) {
      yield NewsPlayVideo();
    }
*/

   }
}
