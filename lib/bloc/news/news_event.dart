import 'package:equatable/equatable.dart';

abstract class NewsEvent extends Equatable {}

class GetNews extends NewsEvent {

  @override
  String toString() => 'GetRestaurantDetail';
}

/*
class PlayVideoClick extends NewsEvent {
  String url;

  PlayVideoClick(String url) {
    this.url = url;
  }

  @override
  String toString() => 'PlayVideoClick';
}


class PlayVideoThumbnail extends NewsEvent {
  String id;

  PlayVideoThumbnail(String id) {
    this.id = id;
  }

  @override
  String toString() => 'PlayVideoThumbnail';
}
*/