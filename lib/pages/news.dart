import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_vegan_app/bloc/news/news_index.dart';
import 'package:go_vegan_app/models/News.dart';
import 'package:go_vegan_app/widgets/videoPlayer.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({ Key key }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NewsPageState();
  }
}


class NewsPageState extends State<NewsPage> {
  NewsBloc newsBloc = NewsBloc();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    newsBloc.dispatch(GetNews());
  }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: newsBloc,
        builder: (BuildContext context, NewsState state) {
          print('state for news $state');
          if (state is NewsUninitialized) {
            return SafeArea(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          if (state is NewsError) {
            return SafeArea(
              child: Center(
                child: Text('Failed to fetch news'),
              ),
            );
          }
          if (state is NewsLoaded) {
            print('news loaded ${state.newsList}');
            if (state.newsList.isEmpty) {
              return SafeArea(
                child: Center(
                  child: Text('No news found'),
                ),
              );
            }
            return _buildListCard(state.newsList);
          }
        });
  }


  Widget _buildListCard(List<News> newsList) {
    return SafeArea(
      child: Container(
          child: ListView.builder(
              padding: EdgeInsets.all(5.0),
              itemCount: newsList.length,
              itemBuilder: (context, i) {
                // if (i.isOdd) return Divider();
                return _buildCard(newsList[i]);
              })
      )
    );
  }

  Widget _buildCard(News oneNews) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.new_releases),
              // ignore: const_eval_throws_exception
              title: Text(oneNews.post_title),
              subtitle: Text(oneNews.post_date),
            ),
            MyVideoPlayer(ytURL: oneNews.youtube_link,)
          ],
        ),
      ),
    );
  }

}
