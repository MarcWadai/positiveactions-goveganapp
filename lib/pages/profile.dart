import 'package:flutter/material.dart';


class Profile extends StatefulWidget {
  Profile({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    final emailField =
        Padding(
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          child:
          Row(
              children: <Widget>[Icon(Icons.perm_identity),
          Expanded(
              child:
              TextField(
                obscureText: false,
                style: style,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(5.0, 0, 0, 0),
                    hintText: "帳號",
                    border: InputBorder.none
                ),
              )
          )],
        ),
    );

    final passwordField = Padding(
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      child:
      Row(
        children: <Widget>[Icon(Icons.lock_open),
        Expanded(
            child:
            TextField(
              obscureText: false,
              style: style,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(5.0, 0, 0, 0),
                  hintText: "密碼",
                  border: InputBorder.none
              ),
            )
        )],
      ),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(12.0),
      color: Theme.of(context).primaryColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        color: Color.fromRGBO(228, 69, 37, 1),
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text("Sorry this feature is not available yet")));
        },
        child: Text("登入",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    final facebookButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(12.0),
      color: Theme.of(context).primaryColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        color: Color.fromRGBO(32, 53, 120 , 1),
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text("Sorry this feature is not available yet")));
        },
        child: Text("Facebook登入",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      body: Center(
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 100.0,
                  child: Center(
                    child: Text('夠維根會員登入',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 30
                        ),
                  )),
                ),
                SizedBox(height: 25.0),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: <Widget>[
                      emailField,
                      // SizedBox(height: 25.0),
                      Row(
                         children: <Widget>[
                           SizedBox(
                             width: 20,
                           ),
                           Expanded(
                             child: Divider(
                               color: Colors.black,
                               height: 5,
                             ),
                           ),
                           SizedBox(
                             width: 20,
                           ),
                         ],
                      ),
                      passwordField,
                    ],
                  ),
                ),
                SizedBox(
                  height: 35.0,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child:
                      facebookButton,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      flex: 1,
                      child:
                      loginButon,
                    )
                  ],
                ),
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}