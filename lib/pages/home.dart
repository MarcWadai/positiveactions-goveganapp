import 'package:flutter/material.dart';
import 'package:go_vegan_app/pages/favorites.dart';
import 'package:go_vegan_app/pages/news.dart';
import 'package:go_vegan_app/pages/profile.dart';
import 'package:go_vegan_app/pages/restaurantsSearch.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin  {

  bool _isMap = false;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 4, vsync: this);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: TabBarView(
          controller: _tabController,
          physics: _isMap ? NeverScrollableScrollPhysics() : PageScrollPhysics(),
          children: [
            NewsPage(),
            RestaurantsSearch(callback: (val) => setState(
                () => _isMap = val
            )),
            Favorites(),
            Profile(),
          ],
        ),
        bottomNavigationBar:
        Container(
          padding: EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(width: 1.0, color: Colors.grey.shade300),
            ),
          ),
          child:  new TabBar(
            controller: _tabController,
            tabs: [
              Tab(
                // icon: new Icon(Icons.list),
                icon: _tabController.index == 0 ? Image.asset('assets/home_selected.png') : Image.asset('assets/home.png'),
              ),
              Tab(
                // icon: new Icon(Icons.home),
                icon: _tabController.index == 1 ? Image.asset('assets/search_selected.png') : Image.asset('assets/search.png'),
              ),
              Tab(
                // icon: new Icon(Icons.favorite),
                icon: _tabController.index == 2 ? Image.asset('assets/favorite_selected.png') : Image.asset('assets/favorite.png'),
              ),
              Tab(
                // icon: new Icon(Icons.perm_identity),)
                icon: _tabController.index == 3 ? Image.asset('assets/account_selected.png') : Image.asset('assets/account.png'),)
            ],
            labelColor: Colors.yellow,
            unselectedLabelColor: Colors.blue,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorPadding: EdgeInsets.all(5.0),
            indicatorColor: Colors.white,
          ),
        )


      );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
