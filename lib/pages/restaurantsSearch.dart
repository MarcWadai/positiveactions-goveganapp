import 'package:flutter/material.dart';
import 'package:go_vegan_app/helpers/database_helpers.dart';
import 'package:go_vegan_app/models/OpeningDays.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:go_vegan_app/pages/restaurantInfo.dart';
import 'package:go_vegan_app/widgets/circleFoodType.dart';
import 'package:go_vegan_app/widgets/searchBar.dart';
import 'package:go_vegan_app/bloc/restaurant_search/restaurant_search_index.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'package:go_vegan_app/helpers/constants.dart' as Constants;
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;

typedef void IsMapCallback(bool val);

class RestaurantsSearch extends StatefulWidget {
  final IsMapCallback callback;

  RestaurantsSearch({Key key, this.callback}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new RestaurantsSearchState();
  }
}

class RestaurantsSearchState extends State<RestaurantsSearch> {
  final RestaurantSearchBloc _searchBloc = RestaurantSearchBloc();
  Completer<GoogleMapController> _controller = Completer();
  MarkerId selectedMarker;

  List<Restaurant> displayedRestaurant = [];
  List<Restaurant> originalList = [];
  List<Restaurant> favoriteRestaurant = [];
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  bool isMap = false;

  @override
  void initState() {
    super.initState();
    _getFavoriteFromDB().then((value) {
      if (value != null) {
        favoriteRestaurant = value;
      }
      _searchBloc.dispatch(GetRestaurants());
    });
  }

  //TODO: Use favorite BLOC instead ///////////////////////////////////////
  Future<List<Restaurant>> _getFavoriteFromDB() async {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    List<Restaurant> favoriteRest = await dbHelper.queryAll();

    return favoriteRest;
  }

  Future<int> _addToFavorite(Restaurant restaurant) async {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    return await dbHelper.insert(restaurant);
  }

  Future<int> _removeFromFavorite(String id) async {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    return await dbHelper.deleteRestaurant(id);
  }

  /////////////////////////////////////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _searchBloc,
        builder: (BuildContext context, RestaurantSearchState state) {
          if (state is SearchUninitialized) {
            return Scaffold(
                body: Center(
              child: CircularProgressIndicator(),
            ));
          }
          if (state is SearchError) {
            return Scaffold(
                body: Center(
              child: Text('failed to fetch posts'),
            ));
          }
          if (state is SearchLoaded) {
            originalList = state.restaurants;
            if (state.restaurants.isEmpty) {
              return Scaffold(
                  body: Center(
                child: Text('No restaurants found'),
              ));
            }
            if (isMap) {
              _udpateMarker(displayedRestaurant, state.restaurants);
            }
            displayedRestaurant = state.restaurants;
            return Scaffold(
              appBar: _makeAppBar(state.restaurants, 0),
              body: isMap
                  ? Center(child: _buildMaps())
                  : Container(child: _buildRestaurants(state.restaurants)),
              floatingActionButton: _floatingButton(isMap),
              floatingActionButtonLocation: isMap
                  ? FloatingActionButtonLocation.endFloat
                  : FloatingActionButtonLocation.endFloat,
            );
          }
          if (state is SearchFilter) {
            if (isMap) {
              _udpateMarker(displayedRestaurant, state.restaurantFilter);
            }
            displayedRestaurant = state.restaurantFilter;

            return Scaffold(
                appBar:
                    _makeAppBar(state.restaurantFilter, state.filterButtonId),
                body: isMap
                    ? Center(child: _buildMaps())
                    : Container(
                        child: _buildRestaurants(state.restaurantFilter)),
                floatingActionButton: _floatingButton(isMap),
                floatingActionButtonLocation: isMap
                    ? FloatingActionButtonLocation.endFloat
                    : FloatingActionButtonLocation.endFloat);
          }

          return Scaffold(
            body: Center(
              child: Text('Nothing'),
            ),
          );
        });
  }

  // common widget ////////////
  Widget _makeAppBar(List<Restaurant> oriRestaurant, int currentButton) {
    return PreferredSize(
      preferredSize: const Size.fromHeight(50.0),
      child: SearchBar(
          originalRestaurant: originalList,
          callback: (String action, List<Restaurant> originalList,
              [String filterValue]) {
            switch (action) {
              case 'RESET':
                _searchBloc.dispatch(FilterReset(originalList));
                break;
              case 'BUTTON':
                _searchBloc.dispatch(
                    FilterWithButton(int.parse(filterValue), originalList));
                break;
              case 'TEXT':
                _searchBloc.dispatch(FilterWithText(filterValue, originalList));
                break;
            }
          }),
    );
  }

  Widget _floatingButton(isMapIcon) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: FloatingActionButton(
          elevation: 0.0,
          child: isMapIcon
              ? Icon(Icons.list, color: Colors.white)
              : Icon(Icons.map, color: Colors.white),
          onPressed: () {
            setState(() {
              isMap = !isMap;
              this.widget.callback(isMap);
            });
          },
        ));
  }

  // Widget for the Maps ////////////
  Widget _buildMaps() {
    CameraPosition _kGooglePlex = CameraPosition(
      target: LatLng(Constants.mapLatOrigin, Constants.mapLngOrigin),
      zoom: 9,
    );

    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: _kGooglePlex,
      myLocationEnabled: false,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
      markers: Set<Marker>.of(markers.values),
    );
  }

  String _getIconMarker(String foodType) {
    switch (foodType) {
      case "1":
        return 'assets/buddisht_marker.png';
      case "2":
        return 'assets/vegan_marker.png';
      case "3":
        return 'assets/vege_marker.png';
      case "4":
        return 'assets/options_marker.png';
      default:
        return 'assets/vegan_marker.png';
    }
  }

  Future<void> _udpateMarker(previousRes, displayedRes) async {
    // do not update the markers if the two list are the same
    if ((markers.length == 0 && displayedRes.length > 0) || (displayedRes.length != previousRes.length)) {
      if (markers.length > 0) {
        markers.clear();
      }
      print(displayedRes);
      for (var r in displayedRes) {
        if (r.lat != null && r.lon != null) {
          MarkerId markerId = MarkerId(r.id);
          Marker marker = await _createMarker(r, markerId);
          markers[markerId] = marker;
        }
      }
      setState(() {
      });
    }
  }

  Future<Uint8List> _getBytesFromAsset(String path, int width) async {
    // ByteData data = await DefaultAssetBundle.of(context).load(path);
    ByteData data = await rootBundle.load(path);

    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  Future<Marker> _createMarker(Restaurant resInfo, MarkerId markerId) async {
    // get marker icon
    final Uint8List markerIcon =
        await _getBytesFromAsset(_getIconMarker(resInfo.food_type), 100);
    // creating a new MARKER
    final Marker marker = Marker(
      markerId: markerId,
      icon: BitmapDescriptor.fromBytes(markerIcon),
      position: LatLng(double.parse(resInfo.lat), double.parse(resInfo.lon)),
      onTap: () {
        _settingModalBottomSheet(context, resInfo);
      },
      /*infoWindow:InfoWindow(title: resInfo.title, snippet: '*',
          onTap: () {
            _onMarkerTapped(resInfo);
          }),*/
    );
    return marker;
  }

  void _settingModalBottomSheet(context, Restaurant resInfo) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            padding: EdgeInsets.fromLTRB(5, 5, 5, 70),
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: Image.network(
                      resInfo.photo_url,
                      fit: BoxFit.contain,
                      width: 50,
                    ),
                    title: Text(resInfo.title),
                    subtitle: _buildOpenClose(resInfo.openingDays.length > 0
                        ? resInfo.openingDays[0]
                        : null),
                    onTap: () => {_onMarkerTapped(resInfo)}),
              ],
            ),
          );
        });
  }

  void _onMarkerTapped(Restaurant restaurant) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => RestaurantInfo(restaurant: restaurant)),
    );
  }

  // Widget for the list ////////////
  Widget _buildRestaurants(List<Restaurant> restaurants) {
    return ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemCount: restaurants.length,
        itemBuilder: (context, i) {
          // if (i.isOdd) return Divider();
          return _buildRow(restaurants[i]);
        });
  }

  Widget _buildRow(Restaurant restaurant) {
    var isFavorite = false;
    if (favoriteRestaurant != null) {
      isFavorite = favoriteRestaurant.any((res) {
        return res.id == restaurant.id;
      });
    }
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: Colors.blue,
        child: Image.network(
          restaurant.photo_url,
        ),
      ),
      title: Row(
        children: <Widget>[
          Expanded(child: Text(restaurant.title)),
          CircleFoodType(foodType: restaurant.food_type)
        ],
      ),
      isThreeLine: true,
      subtitle: Padding(
        padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
        child: Column(
          children: <Widget>[
            Row(children: <Widget>[
              Expanded(
                child: _buildTime(restaurant.openingDays.length > 0
                    ? restaurant.openingDays[0]
                    : null),
              ),
            ]),
            Row(children: <Widget>[
              Expanded(
                child: _buildOpenClose(restaurant.openingDays.length > 0
                    ? restaurant.openingDays[0]
                    : null),
              ),
            ]),
          ],
        ),
      ),
      trailing: IconButton(
        icon: isFavorite
            ? Icon(Icons.favorite, color: Colors.red)
            : Icon(Icons.favorite_border),
        onPressed: () {
          setState(() {
            if (isFavorite) {
              favoriteRestaurant.removeWhere((value) {
                return value.id == restaurant.id;
              });
              _removeFromFavorite(restaurant.id);
            } else {
              favoriteRestaurant.add(restaurant);
              _addToFavorite(restaurant);
            }
          });
        },
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RestaurantInfo(restaurant: restaurant),
          ),
        );
      },
    );
  }

  Widget _buildTime(OpeningDays time) {
    if (time == null) {
      return Text('N/A');
    }
    var timeString =
        ' ${time.hours[0].open_time} - ${time.hours[0].close_time}';
    return Text(timeString);
  }

  Widget _buildOpenClose(OpeningDays time) {
    if (time == null) {
      return Text('N/A');
    }
    var timeString = time.hours[0].closed == "0" ? 'Closed' : 'Open';
    return Text(timeString,
        style: TextStyle(
            color: time.hours[0].closed == "0" ? Colors.red : Colors.green));
  }
}
