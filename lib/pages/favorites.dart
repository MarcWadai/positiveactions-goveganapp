import 'package:flutter/material.dart';
import 'package:go_vegan_app/helpers/database_helpers.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:go_vegan_app/pages/restaurantInfo.dart';

class Favorites extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new FavoritesState();
  }
}


class FavoritesState extends State<Favorites> {

  List<Restaurant> favRestaurant = [];

  @override
  void initState() {
    super.initState();
    _getFavoriteFromDB().then((value) {
      favRestaurant = value;
      this.setState((){});
    });
  }

  Future<List<Restaurant>> _getFavoriteFromDB() async {
    DatabaseHelper dbHelper =  DatabaseHelper.instance;
    List<Restaurant> favoriteRest = await dbHelper.queryAll();
    return favoriteRest;
  }


  Future<bool> _deleteFavoriteFromDB(String id) async {
    DatabaseHelper dbHelper =  DatabaseHelper.instance;
    await dbHelper.deleteRestaurant(id);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    if (favRestaurant != null && favRestaurant.length > 0) {
      return Container(
          child: SafeArea(
              child: _buildRestaurants()
          )
      );
    }
    return Container(
      child: SafeArea(
        child: Center(
          child: Text('No restaurants were added'),
        )
      )
    );

  }

  // Widget for the list ////////////
  Widget _buildRestaurants() {
    return ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemCount: favRestaurant != null ? favRestaurant.length : 0,
        itemBuilder: (context, i) {
          // if (i.isOdd) return Divider();
          return Dismissible(
            // Each Dismissible must contain a Key. Keys allow Flutter to
            // uniquely identify widgets.
            key: Key(favRestaurant[i].id),
            // Provide a function that tells the app
            // what to do after an item has been swiped away.
            onDismissed: (direction) {
              // Remove the item from the data source.
              _deleteFavoriteFromDB(favRestaurant[i].id).then((value) {
                setState(() {
                  favRestaurant.removeAt(i);
                });
              });

              // Then show a snackbar.
              Scaffold.of(context)
                  .showSnackBar(SnackBar(content: Text("${favRestaurant[i].title} dismissed")));
            },
            // Show a red background as the item is swiped away.
            background: Container(color: Colors.red),
            child: _buildRow(favRestaurant[i]),
          );
        });
  }

  Widget _buildRow(Restaurant restaurant) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: Colors.blue,
        child: Image.network(
          restaurant.photo_url,
        ),
      ),
      title: Text(restaurant.title),
      isThreeLine: true,
      subtitle: Padding(
        padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(restaurant.title),
              ],
            ),
            Row(children: <Widget>[
              Text(restaurant.title),
            ]),
          ],
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RestaurantInfo(restaurant: restaurant),
          ),
        );
      },
    );
  }
}