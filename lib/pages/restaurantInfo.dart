import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_vegan_app/bloc/restaurant_detail/restaurants_detail_bloc.dart';
import 'package:go_vegan_app/bloc/restaurant_detail/restaurants_detail_event.dart';
import 'package:go_vegan_app/bloc/restaurant_detail/restaurants_detail_state.dart';
import 'package:go_vegan_app/models/OpeningDays.dart';
import 'package:go_vegan_app/models/Restaurant.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:go_vegan_app/models/RestaurantDetail.dart';
import 'package:go_vegan_app/widgets/circleFoodType.dart';
import 'package:go_vegan_app/widgets/videoPlayer.dart';
import 'package:photo_view/photo_view.dart';

class RestaurantInfo extends StatefulWidget {
  final Restaurant restaurant;

  RestaurantInfo({Key key, @required this.restaurant}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new RestaurantInfoState();
  }
}

class RestaurantInfoState extends State<RestaurantInfo> {
  final RestaurantDetailBloc _detailBloc = RestaurantDetailBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _detailBloc.dispatch(GetRestaurantDetail(widget.restaurant.id));
  }

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create our UI
    return BlocBuilder(
        bloc: _detailBloc,
        builder: (BuildContext context, RestaurantDetailState state) {
          print('state in restaurant info detail $state');
          if (state is DetailUninitialized) {
            return Scaffold(
                appBar: _makeAppbar(widget.restaurant.title),
                body: Center(
                  child: CircularProgressIndicator(),
                ));
          }
          if (state is DetailError) {
            return Scaffold(
                appBar: _makeAppbar(widget.restaurant.title),
                body: Center(
                  child: Text('failed to fetch posts'),
                ));
          }
          if (state is DetailLoaded) {
            if (state.restaurantDetail == null) {
              return Scaffold(
                  appBar: _makeAppbar(widget.restaurant.title),
                  body: Center(
                    child: Text('failed to get restaurant detail'),
                  ));
            }
            return Scaffold(body: _mainLayout(state.restaurantDetail));
          }
        });
  }

  Widget _makeAppbar(String title) {
    return AppBar(
      title: new Text(title, style: TextStyle(color: Colors.white),),

    );
  }

  Widget _mainLayout(RestaurantDetail detail) {
    return Scaffold(
      appBar: _makeAppbar(detail.title),
      body: SafeArea(
          child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
              flex: 1,
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                itemCount: detail.photos.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) => new Container(
                      color: Colors.black,
                      child: Image.network(
                        detail.photos[index].small_url,
                      ),
                    ),
                staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count(index % 3 == 0 ? 4 : 2, 4),
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
              )),
          Expanded(
            flex: 1,
            child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Row(children: <Widget>[
                      Expanded(
                        child: Text(detail.title,
                            style: new TextStyle(
                              fontSize: 25.0,
                              color: Colors.black,
                            )),
                      ),
                      CircleFoodType(foodType: detail.food_type)
                    ]),
                    SizedBox(
                      height: 10,
                    ),
                    Row(children: <Widget>[
                      Expanded(
                          child: Text(detail.address,
                              style: new TextStyle(
                                fontSize: 18.0,
                                color: Colors.blueGrey,
                              )))
                    ]),
                    Row(children: <Widget>[
                      Expanded(child: _buildDayRange(detail))
                    ]),
                    Row(children: <Widget>[
                      Expanded(child: _buildTimeRange(detail))
                    ])
                  ],
                )),
          ),
          Expanded(
            flex: 1,
            child: Container(
              decoration: new BoxDecoration(
                  border: new Border.all(color: Colors.black, width: 2.0),
                  color: Colors.white),
              child: MyVideoPlayer(ytURL: detail.youtube_link),
            ),
          )
        ],
      )),
    );
  }

  Widget _buildTimeRange(RestaurantDetail detail) {
    const style = TextStyle(
      fontSize: 18.0,
      color: Colors.blueGrey,
    );

    if (detail.opening_hours != null && detail.opening_hours.length > 0) {
      return Text(
          '${detail.opening_hours[0].hours[0].open_time} - ${detail.opening_hours[0].hours[0].close_time}',
          style: style);
    }
    return Text('N/A', style: style);
  }

  Widget _buildDayRange(RestaurantDetail detail) {
    const style = TextStyle(
      fontSize: 18.0,
      color: Colors.blueGrey,
    );
    if (detail.opening_hours != null && detail.opening_hours.length > 0) {
      var range = _idtoDay(detail.opening_hours[0].day_start) +
          ' - ' +
          _idtoDay(detail.opening_hours[0].day_end);
      return Text(range, style: style);
    }
    return Text('N/A', style: style);
  }

  String _idtoDay(String id) {
    switch (id) {
      case "1":
        return "Monday";
      case "2":
        return "Tuesday";
      case "3":
        return "Wednesday";
      case "4":
        return "Thursday";
      case "5":
        return "Friday";
      case "6":
        return "Saturday";
      case "7":
        return "Sunday";
    }
  }
}
